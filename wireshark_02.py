#!/usr/bin/env python

import dpkt
import datetime
import socket
import argparse


# convert IP addresses to printable strings 
def inet_to_str(inet):
    # First try ipv4 and then ipv6
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

# add your own function/class/method defines here.


        # sort by time and port number
        # take time list and run probe with width
        # take port id list and run scan with width
        # group by width
        # display
        # run for TCP and UDP

# functions
    # insList
    # sort
    # probe
    # scan


# sort probe, time
# def sortProbe(l):
#   return l.sort(key = *time*) 

# sort scan, port id
# def scanSort(l):
#   return l.sort(key = dport)

# add to probe list
# t = tuple, l = list
def insProbe(t,l):
    list = [t]
    if len(l) == 0:
        l.append(list)
    elif l[len(l)-1][0][0].dport < t[0].dport:
        l.append(list)
    else:
        for i in range(len(l)):
            if l[i][0][0].dport == t[0].dport:
                l[i].append(t)
                break
            elif l[i][0][0].dport > t[0].dport:
                l.insert(i,list)
                break
                
# add to scan list
def insScan(t, l):
    if len(l) == 0:
        l.append(t)
    elif l[len(l)-1][0].dport < t[0].dport:
        l.append(t)
    else:
        for i in range(len(l)):
            if l[i][0].dport >= t[0].dport:
                l.insert(i,t)
                break

# probe
# W_p and N_p
# width and min packets
def probe(l, W_p, N_p):

    head = 0
    tail = 0
    # create list
    retL = []
    temp = []
    
    for list in l:
        while(i < len(list)-2):
            # data is list[][1]
            # compares data + width, if it is greater than next value then increment head
            if list[head][1] + W_p >= list[head + 1][1]:
                # reset tail counter
                tail = head
                while(tail < len(list)-2):
                    # if tail value + width is less than next tail value, break
                    if(list[tail][1] + w < list[tail + 1][1]):
                        break
                    # else increment tail counter
                    else:
                        tail = tail + 1
                temp = list[head:tail + 1]
                if(len(temp) >= N_p):
                    # append packet
                    retL.append(temp)
                head = tail + 1
                        
            else:
                head = head + 1
                
    return retL
        
# scan
# W_s and N_s
# width and min packets
def scan(l, W_s, N_s):

    head = 0
    tail = 0
    # create list
    retL = []
    temp = []
    
    while(i <len(list)-2):
        # data is list[][1]
        # compares data + width, if it is greater than next value then increment head
        if list[head][0].dport + W_s >= list[head + 1][0].dport:
            # reset tail counter
            tail = head
            while(tail < len(list)-2):
                # if tail value + width is less than next tail value, break
                if(list[tail][0].dport + w < list[tail + 1][0].dport):
                    break
                # else increment tail counter
                else:
                    tail = tail + 1
            temp = list[head:tail + 1]
            if(len(temp) >= N_s):
                # append packet
                retL.append(temp)
            head = tail + 1
                        
        else:
            head = head + 1
                
    return retL
        


def main():
    # parse all the arguments to the client
    parser = argparse.ArgumentParser(description='CS 352 Wireshark Assignment 2')
    parser.add_argument('-f', '--filename', type=str, help='pcap file to input', required=True)
    parser.add_argument('-t', '--targetip', type=str, help='a target IP address', required=True)
    parser.add_argument('-l', '--wp', type=int, help='Wp', required=True)
    parser.add_argument('-m', '--np', type=int, help='Np', required=True)
    parser.add_argument('-n', '--ws', type=int, help='Ws', required=True)
    parser.add_argument('-o', '--ns', type=int, help='Ns', required=True)

    # get the parameters into local variables
    args = vars(parser.parse_args())
    file_name = args['filename']
    target_ip = args['targetip']
    W_p = args['wp']
    N_p = args['np']
    W_s = args['ws']
    N_s = args['ns']

    input_data = dpkt.pcap.Reader(open(file_name,'r'))
    
    # tcp lists
    tcpProbeList = []
    tcpScanList = []
    
    #udp lists
    udpProbeList = []
    udpScanList = []

    for timestamp, packet in input_data:
        # this converts the packet arrival time in unix timestamp format
        # to a printable-string
        time_string = datetime.datetime.utcfromtimestamp(timestamp)
        # your code goes here ...
		
		# processing dpkt object into eth, ip, tcp/udp
		eth = dpkt.ethernet.Ethernet(packet)
		ip = eth.data
		

		# store into 2 lists, one by time, one by port number
		
        # sort by port number and time
        # group by distance from next point in list
        # distance is set from -l for seconds and -n for port ID
        # min number of packets in time probe, min number of packets in port ID scan
        # -m, -p
        
        # store -l and -n for min distance
        # W_p, W_s
        # -m, -p are min number of packets
        # N_p, N_s
        
        # variable for current value to compare if min distance or not
        # counter for number of packets
        # store into list and only display if greater than/equal to min num packets
        
        # create a list then keep creating until width is reached, then start again
        
        # run both probe and scan for both TCP and UDP
        
        # probe = time, scan = port ID
        
        # take in file
        # check TCP run then check UDP
        
        # check if it is TCP or UDP
		# store into tcp or udp
		if ip.p == dpkt.ip.IP_PORTO_TCP:
			tcp = ip.data
            tcpTuple = (tcp, timestamp)
            insProbe(tcpTuple, tcpProbeList)
            insScan(tcpTuple, tcpScanList)
            
        elif ip.p == dpkt.ip.IP_PROTO_UDP:
            udp = ip.data
            udpTuple = (udp, timestamp)
            insProbe(udpTuple, udpProbeList)
            insScan(udpTuple, udpScanList)
            
        else:
            print "Invalid packet"
            
            
    tcpProbe = probe(tcpProbeList, W_p, N_p)
    tcpScan = scan(tcpScanList, W_s, N_s)
    udpProbe = probe(udpProbeList, W_p, N_p)
    udpScan = scan(udpScanList, W_s, N_s)
		


    print "CS 352 Wireshark (Part 2)"
    print "Reports for TCP"
    
    print "Found " + str(len(tcpProbe)) + " probes"
    for i in tcpProbe:
        print "Probe: [" + str(len(i)) + " packets]"
        for j in i:
            print "\tPacket [Timestamp " + str(datetime.datetime.utcfromtimestamp(j[1])) + ", Port: " + str(j[0].dport) + "]"
    print "Found " + str(len(tcpScan)) + " scans"
    for i in tcpScan:
        print "Scan: [" + str(len(i)) + " packets]"
        for j in i:
            print "\tPacket [Timestamp " + str(datetime.datetime.utcfromtimestamp(j[1])) + ", Port: " + str(j[0].dport) + "]"
    print "Reports for UDP"
    print "Found " + str(len(udpProbe)) + " probes"
    for i in udpProbe:
        print "Probe: [" + str(len(i)) + " packets]"
        for j in i:
            print "\tPacket [Timestamp " + str(datetime.datetime.utcfromtimestamp(j[1])) + ", Port: " + str(j[0].dport) + "]"
    print "Found " + str(len(udpScan)) + " scans"
    for i in udpScan:
        print "Scan: [" + str(len(i)) + " packets]"
        for j in i:
            print "\tPacket [Timestamp " + str(datetime.datetime.utcfromtimestamp(j[1])) + ", Port: " + str(j[0].dport) + "]"
    		
# execute a main function in Python
if __name__ == "__main__":
    main()
